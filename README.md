# pocketBonsai
Procedural trees on your mobile

## lsystem documentation

Is here until I break the lsystem out into its own package

### Basic commands

I have adapted the classic l-system commands into something I can remember.  e.g. Turning is now Pitch, Roll, and Yaw.  Other commands are adapted to work with OpenGL ES 2.0, e.g. the polygon drawing functions.

#### Turning

Now uses aircraft terminology

P,p     Pitch up and down (Nose up, nose down).
R,r     Roll right and left (Tilt wings)
Y,y     Yaw right and left (Turn without tilting)
HR      Rotation hinge.  Hinges are in development, so for now it just changes angle every frame

#### Movement

F,f     Jump forward and back by one unit.  Don't draw anything
TF      Draw a triangle and jump forward.  New position is in contact with the triangle

#### Other

[ ]     Push/pop stack.  [ Saves the transform matrix, and attributes like colour and angle.  ] Restores these values.
T       Draw a triangle, don't move
S,s     Shrink/expand by 0.5/2.0

#### Functions

Colour255,0,255     Set the current colour (numbers are R,G,B)
Scale(5.0,1.0,10.0) Scale along (x,y,z) axes of the transform matrix
A1.5707             Set the turn angle, in radiians

#### Polygons

Note:  L-Engine does not support the original { . . . } polygon syntax from ABOP.  L-Engine only draws triangles, so you will have to break polygons into triangles yourself.

.       Adds a vertex to the vertex list.  L-Engine creates lsystems by pushing vertexes onto a vetex array.  Even commands like 'T' just push 3 vertexes onto the array.  When drawing, openGL just reads 3 vertexes at a time and draws a triangle.

reverseTriangle Reverses the order of the triangle you just created (it swaps the last two vertexes).  Useful because sometimes you cannot change the l-system to make the triangles wind correctly, so you can change the winding order afterwards.

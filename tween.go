// animations
package main

import "time"
//import "log"

var animSleep time.Duration = 15

func tween_clock(v *float32, start, end, duration float32, startTime time.Time) {
    time.Sleep(animSleep*time.Millisecond)
    t := float32(time.Since(startTime).Seconds())/duration
    *v = start + t*(end-start)
    if t>1.0 {
        t=0.0
    }
    tween_clock(v, start, end, duration, startTime)
}


func tween_linear(v *float32, start, end, duration float32, startTime time.Time) {
    time.Sleep(animSleep*time.Millisecond)
    t := float32(time.Since(startTime).Seconds())/duration
    *v = start + t*(end-start)
    if t>1.0 {
        return
    } else {
        tween_linear(v, start, end, duration, startTime)
    }
}

func start_linear_animation(v *float32, start, end, duration float32) {
    go tween_linear(v, start, end, duration, time.Now())
}

func start_clock(v *float32, start, end, duration float32) {
    go tween_clock(v, start, end, duration, time.Now())
}

package main

import (
	"regexp"
	"strconv"

	"encoding/binary"
"fmt"
//"log"
	"os"
	"strings"

	"github.com/donomii/goStringTable"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/gl"
)

import "github.com/go-gl/mathgl/mgl32"

var trans mgl32.Mat4
var recursion int = 4
var threeD bool = false
var polyCount int
var clock float32 = 0.0

func rewrite(aString []string, rules map[string][]string) []string {

	//fmt.Println(aString)
	var outString []string
	for _, orig := range aString {
		new, ok := rules[orig]
		//fmt.Println(orig, new)
		if !ok {
			outString = append(outString, []string{orig}...)
		} else {
			outString = append(outString, new...)
		}
	}
	return outString
}

func pushState(aStack []mgl32.Mat4, aVal mgl32.Mat4) []mgl32.Mat4 {
	return append(aStack, aVal)
}

type attribs struct {
	angle       float32
	red         float32
	green       float32
	blue        float32
	alpha       float32
	useLighting bool
	mirror      bool
}

var attribStack []attribs
var modelReady *goStringTable.SymTable

func L_init() {
	modelReady = goStringTable.New()

}

func pushAttribs(aStack []attribs, aVal attribs) []attribs {
	return append(aStack, aVal)
}

func popAttribs(aStack []attribs, aVal attribs) ([]attribs, attribs) {
	//fmt.Printf("Stack length: %f\n", len(aStack))
	//fmt.Printf("Stack : %v\n", aStack)
	if len(aStack) < 1 {
		//fmt.Println("Pop called on empty stack!")
		return []attribs{}, aVal
		os.Exit(0)
	}
	if len(aStack) == 1 {
		return []attribs{}, aStack[0]
	}
	if len(aStack) == 2 {
		return []attribs{aStack[0]}, aStack[1]
	}
	if len(aStack) == 3 {
		return []attribs{aStack[0], aStack[1]}, aStack[2]
	}
	//if (len(aStack) == 3 ) {
	//	return aStack[0:1], aStack[2]
	//}
	return aStack[:len(aStack)-1], aStack[len(aStack)-1]
}

func popState(aStack []mgl32.Mat4, aVal mgl32.Mat4) ([]mgl32.Mat4, mgl32.Mat4) {
	//fmt.Printf("Stack length: %f\n", len(aStack))
	//fmt.Printf("Stack : %v\n", aStack)
	if len(aStack) < 1 {
		fmt.Println("Pop called on empty stack!")
		//return []mgl32.Mat4{}, aVal
		os.Exit(0)
	}
	if len(aStack) == 1 {
		return []mgl32.Mat4{}, aStack[0]
	}
	if len(aStack) == 2 {
		return []mgl32.Mat4{aStack[0]}, aStack[1]
	}
	if len(aStack) == 3 {
		return []mgl32.Mat4{aStack[0], aStack[1]}, aStack[2]
	}
	//if (len(aStack) == 3 ) {
	//	return aStack[0:1], aStack[2]
	//}
	return aStack[:len(aStack)-1], aStack[len(aStack)-1]
}

func compose(a, b mgl32.Mat4) mgl32.Mat4 {
	return a.Mul4(b)
}

func compose3(a, b, c mgl32.Mat4) mgl32.Mat4 {
	t := b.Mul4(c)
	return a.Mul4(t)
}

func testPushPop() {

}

//Splitstring
func s(str string) []string {
	str = regexp.MustCompile("\n").ReplaceAllLiteralString(str, " ")
	str = regexp.MustCompile("\r").ReplaceAllLiteralString(str, " ")
	str = regexp.MustCompile("\t").ReplaceAllLiteralString(str, " ")
	str = regexp.MustCompile("  ").ReplaceAllLiteralString(str, " ")
	str = regexp.MustCompile("  ").ReplaceAllLiteralString(str, " ")
	return strings.Split(str, " ")
}

type ruleMeta struct {
	iterations   int
	x            float32
	y            float32
	style        gl.Enum
	backfaceCull bool
	useLighting  bool
}

func ruleBookMeta() map[string]ruleMeta {
	ret := map[string]ruleMeta{
		"Quad":        ruleMeta{iterations: 3, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"risingVine":  ruleMeta{iterations: 10, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Koch3":       ruleMeta{iterations: 4, x: 0.5, y: 0.5, style: gl.LINES},
		"Koch2":       ruleMeta{iterations: 4, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"3DTreeLeafy": ruleMeta{iterations: 3, x: 0.5, y: 0.9, style: gl.TRIANGLES},
		"3DTree3":     ruleMeta{iterations: 4, x: 0.5, y: 0.9, style: gl.TRIANGLES},
		"Tree3":       ruleMeta{iterations: 4, x: 0.5, y: 0.9, style: gl.LINE_LOOP},
		"starburst":   ruleMeta{iterations: 10, x: 0.5, y: 0.7, style: gl.TRIANGLES},
		"leaf2":       ruleMeta{iterations: 7, x: 0.5, y: 0.7, style: gl.TRIANGLE_FAN},
		"leaf":        ruleMeta{iterations: 7, x: 0.5, y: 0.7, style: gl.TRIANGLES},
		"ilake":       ruleMeta{iterations: 4, x: 0.5, y: 0.7, style: gl.TRIANGLES},
		"lineStar":    ruleMeta{iterations: 8, x: 0.5, y: 0.7, style: gl.LINE_STRIP},
		"KIomega":     ruleMeta{iterations: 3, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Gosper":      ruleMeta{iterations: 6, x: 0.5, y: 0.5, style: gl.LINE_LOOP},
		"Sierpinksi":  ruleMeta{iterations: 7, x: 0.5, y: 0.5, style: gl.LINE_LOOP},
		"Square":      ruleMeta{iterations: 1, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Square1":     ruleMeta{iterations: 5, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Flower":      ruleMeta{iterations: 5, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Flower12":    ruleMeta{iterations: 5, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Flower11":    ruleMeta{iterations: 5, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Flower10":    ruleMeta{iterations: 5, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Plant":       ruleMeta{iterations: 5, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"FlowerField": ruleMeta{iterations: 30, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"WedgeLeaf":   ruleMeta{iterations: 1, x: 0.5, y: 0.5, style: gl.LINE_LOOP},
		"Face":        ruleMeta{iterations: 4, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Arrow":       ruleMeta{iterations: 4, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Tetrahedron": ruleMeta{iterations: 2, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Circle":      ruleMeta{iterations: 10, x: 0.5, y: 0.5, style: gl.TRIANGLES},
		"Prism":      ruleMeta{iterations: 3, x: 0.5, y: 0.5, style: gl.TRIANGLES, backfaceCull: true, useLighting: true},
		"Prism1":      ruleMeta{iterations: 3, x: 0.5, y: 0.5, style: gl.TRIANGLES, backfaceCull: true},
		"Icosahedron": ruleMeta{iterations: 4, x: 0.5, y: 0.5, style: gl.TRIANGLES, backfaceCull: true, useLighting: true},
	}
	return ret
}

func ruleBook() map[string]map[string][]string {
	rb := map[string]map[string][]string{
		"ilake": map[string][]string{
			"ilake":      strings.Split("A1.57 ilakeiter Y ilakeiter Y ilakeiter Y ilakeiter", " "),
			"ilakeiter":  strings.Split("A1.57 ilakeiter Y ilakeiterf y ilakeiter ilakeiter Y ilakeiter Y ilakeiter ilakeiter Y ilakeiter ilakeiterf Y ilakeiter ilakeiter y ilakeiterf Y ilakeiter ilakeiter y ilakeiter y ilakeiter ilakeiterf y ilakeiter ilakeiter ilakeiter", " "),
			"ilakeiterf": strings.Split("TF ilakeiterf ilakeiterf ilakeiterf ilakeiterf ilakeiterf ilakeiterf", " "),
		},
		"Quad": map[string][]string{
			"Quad": strings.Split("Q", " "),
		},
		"Koch3": map[string][]string{
			"Koch3": strings.Split("R A1.57 TF y TF y TF y TF", " "),
			"TF":    strings.Split("TF TF y TF y TF y TF y TF y TF Y TF", " "),
		},
		"Tree3": map[string][]string{
			"Tree3": strings.Split("A0.3926991 TF", " "),
			"TF":    strings.Split("TF TF y [ y TF Y TF Y TF ] Y [ Y TF y TF y TF ]", " "),
		},
		"3DTree3": map[string][]string{
			"3DTree3": strings.Split("Colour255,233,155 A0.3926991 TF", " "),
			"TF":      strings.Split("TF TF y r [ y TF Y TF Y TF ] Y [ Y TF y TF y TF ]", " "),
		},
		"3DTreeLeafy": map[string][]string{
			"3DTreeLeafy": strings.Split("Colour0,255,255 A0.3926991 TF", " "),
			"TF":          strings.Split("TF TF y r le Colour0,255,255 [ y TF Y TF Y TF [ s s s s s s leaf ] Colour0,255,255  ] Y [ Y TF y TF y TF [ s s s s s s leaf ] Colour0,255,255 ]  Colour0,255,255", " "),
		},
		"starburst": map[string][]string{
			"starburst": strings.Split("A0.35 [ lA ] [ lB ]", " "),
			"lA":        strings.Split("[ y lA ] lC", " "),
			"lB":        strings.Split("[ Y lB ] lC", " "),
			"lC":        strings.Split("TF lC", " "),
		},
		"leaf": map[string][]string{
			"leaf": strings.Split("A0.35 Colour0,255,0 [ lA ] [ lB ]", " "),
			"lB":   strings.Split("[ Y lB { Colour0,155,0 . ] Colour0,255,0 . lC Colour0,155,0 . }", " "),
			"lA":   strings.Split("[ y lA { Colour0,155,0 . ] Colour0,155,0 . lC Colour0,255,0 . reverseTriangle }", " "),
			"lC":   strings.Split("F lC", " "),
		},
		"lineStar": map[string][]string{
			"lineStar": strings.Split("A0.35 Colour0,255,0 [ lA ] [ lB ]", " "),
			"lA":       strings.Split("[ y lA { . ] . lC . }", " "),
			"lB":       strings.Split("[ Y lB { . ] . lC . }", " "),
			"lC":       strings.Split("F lC", " "),
		},
		"orient": map[string][]string{
			"orient": strings.Split("TF A1.50 Colour255,0,0 starburst Y Y starburst P F F F Colour0,255,0 starburst Y Y starburst", " "),
		},
		"leaf2": map[string][]string{
			"leaf2": strings.Split("A0.35 Colour0,255,0 . [ lA ] [ lB ]", " "),
			"lA":    strings.Split("[ y lA ] lC .", " "),
			"lB":    strings.Split("[ Y lB ] lC .", " "),
			"lC":    strings.Split("F lC", " "),
		},
		"risingVine": map[string][]string{
			"risingVine": strings.Split("[ A0.35 AA ] A1.57 r r y y [ A0.35 AA ]", " "),
			"AA":         strings.Split("TF TF TF TF s AA", " "),
		},
		"KIomega": map[string][]string{
			"KIomega": strings.Split("A1.57 s s s s s TF y TF y TF y TF y", " "),
			"TF":      strings.Split("TF y TF Y TF Y TF TF y TF y TF Y TF", " "),
		},
		"Gosper": map[string][]string{
			"Gosper":  strings.Split("Colour255,255,255 A1.0472 Gosperl", " "),
			"Gosperl": strings.Split(". F Gosperl Y Gosperr Y Y Gosperr y Gosperl y y Gosperl Gosperl y Gosperr Y", " "),
			"Gosperr": strings.Split(". F y Gosperrlr Y Gosperr Gosperr Y Y Gosperr Y Gosperl y y Gosperl y Gosperr", " "),
		},
		"Sierpinksi": map[string][]string{
			"Sierpinksi": strings.Split("A1.0472 Sierpr", " "),
			"Sierpl":     strings.Split(". F Sierpr Y Sierpl Y Sierpr", " "),
			"Sierpr":     strings.Split(". F Sierpl y Sierpr y Sierpl", " "),
		},
		"Koch2": map[string][]string{
			"Koch2": strings.Split("A1.57 y TF", " "),
			"TF":    strings.Split("TF Y TF y TF y TF Y TF T F", " "),
		},
		"Square": map[string][]string{
			"Square": strings.Split("Colour0,0,255 A1.57 TF y y TF", " "),
		},
		"Plant": map[string][]string{
			"Plant":     strings.Split("A0.314159 Colour139,69,19 Plant1", " "),
			"Plant1":    strings.Split("A0.314159 Colour139,69,19 internode Y [ Plant1 + Flower ] y y R R [ y y s s s leaf ] internode [ Y Y s s s leaf ] y [ Plant1 Flower ] Y Y Plant1 Flower", " "),
			"internode": strings.Split("TF seg [ R R p p s s leaf ] [ R R P P s s leaf ] TF seg", " "),
			"seg":       strings.Split("seg TF seg", " "),
		},
		"Flower": map[string][]string{
			"Flower":  strings.Split("A0.314159 Colour139,69,19 [ Pedicel RotateColour Y Wedge Y Y Y Y Wedge Y Y Y Y Wedge Y Y Y Y Wedge Y Y Y Y Wedge ]", " "),
			"Pedicel": strings.Split("TF TF", " "),
			"Wedge":   strings.Split("[ Colour0,255,0 P TF ] [ WedgeLeaf ]", " "),
		},
		"Flower12": map[string][]string{
			"Flower12": strings.Split("A0.523598 Colour139,69,19 [ Pedicel RotateColour Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge  ]", " "),
			"Pedicel":  strings.Split("TF TF", " "),
			"Wedge":    strings.Split("[ Colour0,255,0 P TF ] [ WedgeLeaf ]", " "),
		},
		"Flower11": map[string][]string{
			"Flower11": strings.Split("A0.523598 Colour139,69,19 [ Pedicel RotateColour Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge ]", " "),
			"Pedicel":  strings.Split("TF TF", " "),
			"Wedge":    strings.Split("[ Colour0,255,0 P TF ] [ WedgeLeaf ]", " "),
		},
		"Flower10": map[string][]string{
			"Flower10": strings.Split("A0.523598 Colour139,69,19 [ Pedicel RotateColour Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge Y Wedge ]", " "),
			"Pedicel":  strings.Split("TF TF", " "),
			"Wedge":    strings.Split("[ Colour0,255,0 P TF ] [ WedgeLeaf ]", " "),
		},
		"FlowerField": map[string][]string{
			"FlowerField": strings.Split("Flower A1.57 s s P y [ spiral ]", " "),
			"column":      strings.Split("column [ A0.75 Y A1.5708 [ row A1.57 ] ] F F F", " "),
			"columnb":     strings.Split("columnb [ A0.75 Y A1.5708 [ row A1.57 ] ] f f f", " "),
			"row":         strings.Split("row A0.2 p F [ Flower ]", " "),
			"spiral":      strings.Split("TF [ Flower ] A0.2 Y Scale(0.9,0.9,0.9) spiral", " "),
		},
		"WedgeLeaf": map[string][]string{
			//"WedgeLeaf" : strings.Split("p p A0.6 y Colour255,255,255 . [ F Y . F A1.5708 Y Y A0.6 y Colour255,255,255 . . F Y Colour255,255,255 Colour255,200,200 . F ] Colour255,255,255 . A0.523598", " "),
			"WedgeLeaf": strings.Split("p p A0.6 y Colour255,255,255 . [ F Y . F A1.5708 Y Y A0.6 y Colour255,255,255 . . F Y Colour255,255,255 Colour255,200,200 . F ] Colour255,255,255 .", " "),
		},
		"Square1": map[string][]string{
			//"chunk" : strings.Split("Colour255,0,0 A1.5707 [ .  F Y . F Y . . F Y . F Y . ]", " "),
			"chunk1":  strings.Split("A1.5707 Colour255,255,255 [ [ P s s F . ] F y Colour0,255,0 . F y . . F y . F y [ P s s F Colour255,255,255 . ] ]", " "),
			"chunk":   strings.Split("A1.5707 Colour255,255,255 s s s s [ chunk1 ] Y  Colour255,255,255 [ chunk1 ] Y [ chunk1 ] Y [ chunk1 ]", " "),
			"Square1": strings.Split("[ chunk ]", " "),
		},
		"Face": map[string][]string{
			"chunk": strings.Split("Colour255,0,0 A1.5707 [ . [ F . ] Y [ F  . ] ]", " "),
			"Face":  strings.Split("s s s s s s chunk Y chunk Y chunk Y chunk", " "),
		},
		"Arrow": map[string][]string{
			"chunk": strings.Split("Colour255,0,0 A1.5707 [ [ F op [ F Colour0,255,0 op ] ] Y [ Colour0,0,255 F  op ] ]", " "),
			"Arrow": strings.Split("s s s s chunk R chunk R chunk R chunk", " "),
		},
		"Tetrahedron": map[string][]string{
			"c1": s("[ F P F Y F . ]"),
			"c2": s("[ f P f Y F . ]"),
			"c3": s("[ f P F Y f . ]"),
			"c4": s("[ F P f Y f . ]"),
			"Tetrahedron": s(`A1.5707 Colour255,0,0
            [ c1 c2 c3 ] Colour0,255,0
            [ c1 c3 c4 ] Colour0,0,255
            [ c1 c2 c4 ] Colour255,255,255
            [ c4 c3 c2 ]
        `),
		},
		"Prism": map[string][]string{
			"c1": s("[ . ]"),
			"c2": s("[ F . ]"),
			"c3": s("[ P F . ]"),
			"c4": s("[ Y F y . ]"),
			"c5": s("[ Y F y F . ]"),
			"c6": s("[ Y F y P F . ]"),
			"Prism": s(`Colour0,255,0  A1.5707 hs y F Y p F P hS
                [ c1 c2 c3 ] [ c1 c4 c2 ] [ c1 c3 c4 ]
                [ c4 c6 c5 ] [ c4 c5 c2 ] [ c4 c3 c6 ]
                [ c2 c5 c6 ] [ c6 c3 c2 ]
                hs F Y F y hS p`),
        },
		"Prism1": map[string][]string{
			"c1": s("[ . ]"),
			"c2": s("[ F . ]"),
			"c3": s("[ P F . ]"),
			"c4": s("[ Y F y . ]"),
			"c5": s("[ Y F y F . ]"),
			"c6": s("[ Y F y P F . ]"),
			"Prism1": s(`Colour255,0,0 A1.5707 hs y F Y p F P hS
                [ c1 c2 c3 ] [ c1 c4 c2 ] [ c1 c3 c4 ]
                [ c4 c6 c5 ] [ c4 c5 c2 ] [ c4 c3 c6 ]
                [ c2 c5 c6 ] [ c6 c3 c2 ]
                hs F Y F y hS p`),
        },
		"Circle": map[string][]string{
			"spoke":  s("spoke Y [ F F s s s Tetrahedron ]"),
			"Circle": s(`A0.71 spoke`),
		},
		"Icosahedron": map[string][]string{
			"patch1": s(`
                        [ F F Y F . ] [ P F F p F . ] [ Y F F P F . ] reverseTriangle
                        [ F F y F . ] [ P F F p F . ] [ y F F P F . ]
                    `),
			"ring": s(`
                Colour255,0,0 [ F [ F [ Y F . ] [ y F . ] ] [ P F F . ] ] reverseTriangle
                Colour0,255,0 [ F [ F [ Y F . ] [ y F . ] ] [ p F F . ] ]
                Y Y
                Colour0,0,255 [ F [ F [ Y F . ] [ y F . ] ] [ P F F . ] ] reverseTriangle
                Colour255,0,255 [ F [ F [ Y F . ] [ y F . ] ] [ p F F . ] ]
`),
			"Icosahedron": s(`A1.5707
                    [ Y Y p R R p
                    Colour255,255,255
                    patch1
                ]
                [ ring ]
                [
                    Y P
                    ring
                ]
                [
                    Y R
                    ring
                ]
                [
                    Colour255,255,0
                    patch1
                    R R
                    Colour255,0,255
                    patch1
                    P P
                    Colour0,255,255
                    patch1
                ]
                `),
		},
	}
	return rb

}

func rules() map[string][]string {
	rules := map[string][]string{
		"st": strings.Split("R Y T F", " "),
		"le": strings.Split("[ y y P P Colour0,255,0 T y R R R Colour0,255,0 T ]", " "),

		"Koch10":  strings.Split("A1.57 Koch10l", " "),
		"Koch10l": strings.Split("Koch10l Y Koch10r Y", " "),
		"Koch10r": strings.Split("y Koch10l y Koch10r", " "),
		"stem":    []string{"F", "A0.35", "R", "[", "s", "square", "stem", "]", "[", "A0.75", "y", "s", "square", "stem", "]"},
	}
	return rules
}

var compiled []string = nil

var clrClr float32 = 1

func runRules(commands []string, rules map[string][]string, recursion int) []string {

	//if (compiled ==nil) {
	//fmt.Println("Compiling")
	for i := 0; i < recursion; i++ {
		//fmt.Println("Recursing")
		i = i + 0 //Must use the loop variable or the go compiler complains
		commands = rewrite(commands, rules)
	}
	compiled = commands
	//} else {
	////fmt.Println("Using compiled")
	//commands =  compiled
	//clrClr = 0
	//}
	return commands
}

func runRuleset(aString []string, ruleset map[string]map[string][]string) []string {
	var outString []string
	for i := 0; i < 50; i++ {
		outString = []string{}
		for _, orig := range aString {
			new, ok := ruleset[orig]
			if ok {
				//fmt.Println("Replace:")
				//fmt.Println(orig, new)
				outString = append(outString, runRules([]string{orig}, new, ruleBookMeta()[orig].iterations)...)
			} else {
				//fmt.Println(orig, new)
				outString = append(outString, []string{orig}...)
			}
		}
		//fmt.Println("Iterate:")
		//fmt.Println(outString)
		aString = outString
	}
	return outString
}

var r float32 = 0.0

var quadTexAlignData = f32.Bytes(binary.LittleEndian,
	0.0, 0.0, // top left
	0.0, 1.0, // top left
	1.0, 0.0, // top left
	0.0, 1.0, // top left
	1.0, 1.0, // top left
	1.0, 0.0, // top left
)

func quad(glctx gl.Context, trans mgl32.Mat4) []float32 {
	var triangleData = []float32{
		-1.0, 1.0, 0.0, // top lef
		-1.0, -1.0, 0.0, // bottom left
		1.0, 1.0, 0.0, // bottom right
		-1.0, -1.0, 0.0, // bottom right
		1.0, -1.0, 0.0, // top left
		1.0, 1.0, 0.0, // bottom right
	}
	return triangleData
}

func tr(glctx gl.Context, trans mgl32.Mat4) []float32 {
	polyCount = polyCount + 1
	vec := mgl32.Vec4{0.0, 1.0, 0.0, 1}
	p1 := trans.Mul4x1(vec)
	vec = mgl32.Vec4{-0.1, 0.0, 0.0, 1}
	p2 := trans.Mul4x1(vec)
	vec = mgl32.Vec4{0.1, 0.0, 0.0, 1}
	p3 := trans.Mul4x1(vec)
	newTri := []float32{p1[0], p1[1], p1[2], p2[0], p2[1], p2[2], p3[0], p3[1], p3[2]}
	return newTri
}

func line(glctx gl.Context, trans mgl32.Mat4) []float32 {
	polyCount = polyCount + 1
	vec := mgl32.Vec4{0.0, 1.0, 0.0, 1}
	p1 := trans.Mul4x1(vec)
	vec = mgl32.Vec4{-0.1, 0.0, 0.0, 1}
	p2 := trans.Mul4x1(vec)
	newTri := []float32{p1[0], p1[1], p1[2], p2[0], p2[1], p2[2]}
	return newTri
}

func old_point(glctx gl.Context, trans mgl32.Mat4) []float32 {
	polyCount = polyCount + 1
	vec := mgl32.Vec4{0.0, 1.0, 0.0, 1}
	p1 := trans.Mul4x1(vec)
	newVert := []float32{p1[0], p1[1], p1[2]}
	return newVert
}

func point(glctx gl.Context, trans mgl32.Mat4) []float32 {
	polyCount = polyCount + 1
	vec := mgl32.Vec4{0.0, 0.0, 0.0, 1}
	p1 := trans.Mul4x1(vec)
	newVert := []float32{p1[0], p1[1], p1[2]}
	return newVert
}

func abs(n float32) float32 {
	if n < 0.0 {
		return -1.0 * n
	}
	return n
}

func reverse3(t []float32) {
	s := len(t) - 6
	a := mgl32.Vec3{t[s], t[s+1], t[s+2]}
	b := mgl32.Vec3{t[s+3], t[s+4], t[s+5]}

	t[s], t[s+1], t[s+2] = b[0], b[1], b[2]
	t[s+3], t[s+4], t[s+5] = a[0], a[1], a[2]
}

func calcNorm(a, b, c mgl32.Vec4) mgl32.Vec3 {
	aa := mgl32.Vec3{a[0], a[1], a[2]}
	bb := mgl32.Vec3{b[0], b[1], b[2]}
	cc := mgl32.Vec3{c[0], c[1], c[2]}
	A := bb.Sub(aa)
	//A = A.Normalize()
	B := cc.Sub(aa)
	//B = B.Normalize()
	fmt.Println("Crossing ", B, " with ", A)
	//C := B.Cross(A)
	C := mgl32.Vec3{}
	C[0] = (A.Y()*B.Z() - (A.Z() * B.Y()))
	C[1] = (A.Z()*B.X() - (A.X() * B.Z()))
	C[2] = (A.X()*B.Y() - (A.Y() * B.X()))
	fmt.Println("Normalizing ", C)
	return C.Normalize()
}

func buildTexBuff(glctx gl.Context) gl.Buffer {
	tbuf := glctx.CreateBuffer()
	glctx.BindBuffer(gl.ARRAY_BUFFER, tbuf)
	fmt.Printf("texAlignData: %V\n", texAlignData)
	glctx.BufferData(gl.ARRAY_BUFFER, texAlignData, gl.STATIC_DRAW)
	return tbuf
}

func buildIt(glctx gl.Context, seed string, name string) {
	fmt.Println("Building ", name)
	normBuf := []float32{}
	buf := glctx.CreateBuffer()
	colbuf := glctx.CreateBuffer()
	normbuf := glctx.CreateBuffer()
	checkGlErr(glctx)
	vBuffs[name] = buf
	vColBuffs[name] = colbuf
	vNormBuffs[name] = normbuf
	trans = mgl32.Ident4()
	triBuf, colBuf := draw(glctx, camera.ViewMatrix(), seed, trans, true, false)
	vTrisf[name] = triBuf
	vColsf[name] = colBuf
	for i := 0; i <= len(triBuf)-9; i = i + 9 {
		fmt.Println("TriBuf at i:", i, triBuf[i:i+8])
		a := mgl32.Vec4{triBuf[i], triBuf[i+1], triBuf[i+2]}
		b := mgl32.Vec4{triBuf[i+3], triBuf[i+4], triBuf[i+5]}
		c := mgl32.Vec4{triBuf[i+6], triBuf[i+7], triBuf[i+8]}
		fmt.Println(a, b, c)
		norm := calcNorm(a, b, c)
		fmt.Println(norm)
		fmt.Println("")
		normBuf = append(normBuf, norm[0], norm[1], norm[2])
		normBuf = append(normBuf, norm[0], norm[1], norm[2])
		normBuf = append(normBuf, norm[0], norm[1], norm[2])
	}
	//fmt.Println("Tribuffs: ", len(triBuf), " Normals: ", len(normBuf))
	//FIXME we seem to be not calculating the very last triangle in a model...
	vNormsf[name] = normBuf
	vTris[name] = f32.Bytes(binary.LittleEndian, vTrisf[name]...)
	vCols[name] = f32.Bytes(binary.LittleEndian, vColsf[name]...)
	vNorms[name] = f32.Bytes(binary.LittleEndian, vNormsf[name]...)
	vMeta[name] = vertexMeta{coordsPerVertex: 3, vertexCount: len(vTrisf[name]) / 3}
	glctx.BindBuffer(gl.ARRAY_BUFFER, vBuffs[name])
	checkGlErr(glctx)
	glctx.BufferData(gl.ARRAY_BUFFER, vTris[name], gl.STATIC_DRAW)
	checkGlErr(glctx)
	glctx.BindBuffer(gl.ARRAY_BUFFER, vColBuffs[name])
	checkGlErr(glctx)
	glctx.BufferData(gl.ARRAY_BUFFER, vCols[name], gl.STATIC_DRAW)
	checkGlErr(glctx)
	glctx.BindBuffer(gl.ARRAY_BUFFER, vNormBuffs[name])
	checkGlErr(glctx)
	glctx.BufferData(gl.ARRAY_BUFFER, vNorms[name], gl.STATIC_DRAW)
	checkGlErr(glctx)
	//fmt.Printf("%v:Coords per vertex: %v, vertexes: %v\n",name, vMeta[name].coordsPerVertex, vMeta[name].vertexCount)
	//fmt.Printf("%v:Vertex array len: %v, Vertex buffer len: %v\n",name, len(vTrisf[name]), len(vTris[name]))
	if len(vTris[name]) == 0 {
		fmt.Printf("Error!  Shape %v has no vertexes, so nothing will be drawn!\n", name)
	}
	modelReady.LookupOrCreate(name)
}

func buildAll(glctx gl.Context) {
	for _, v := range gallery {
		buildIt(glctx, v, v)
	}
	for k := range ruleBook() {
		buildIt(glctx, k, k)
	}

}

func lsysPaint(glctx gl.Context, sz size.Event) {
	//fmt.Printf("Starting paint\n")
	glctx.Enable(gl.BLEND)
	glctx.Viewport(0, 0, screenWidth, screenHeight)
	glctx.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	glctx.Enable(gl.DEPTH_TEST)
	glctx.DepthFunc(gl.LEQUAL)
	glctx.DepthMask(true)
	glctx.ClearColor(0, 0, 0, 1)
	glctx.UseProgram(program) //FIXME - may cause graphics glitches
	polyCount = 0
	glctx.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	if reCalcNeeded {
		reCalcNeeded = false
	}
	if threeD {

		glctx.Viewport(0, 0, sz.WidthPx/2, sz.HeightPx)
		trans = mgl32.Ident4()
		leftEye := camera.ViewMatrix().Mul4(mgl32.Translate3D(0.05, 0.0, 0.0))
		draw(glctx, leftEye, gallery[selection], trans, false, false)
		trans = mgl32.Ident4()
		trans = compose(mgl32.Scale3D(1.0, -1.0, 1.0), trans)
		leftEye = camera.ViewMatrix().Mul4(mgl32.Translate3D(0.05, 0.0, 0.0))
		draw(glctx, leftEye, gallery[selection], trans, false, true)

		glctx.Viewport(sz.WidthPx/2, 0, sz.WidthPx/2, sz.HeightPx)
		trans = mgl32.Ident4()
		rightEye := camera.ViewMatrix().Mul4(mgl32.Translate3D(-0.05, 0.0, 0.0))
		draw(glctx, rightEye, gallery[selection], trans, false, false)
		trans = mgl32.Ident4()
		trans = compose(mgl32.Scale3D(1.0, -1.0, 1.0), trans)
		rightEye = camera.ViewMatrix().Mul4(mgl32.Translate3D(-0.05, 0.0, 0.0))
		draw(glctx, rightEye, gallery[selection], trans, false, true)

	} else {

		//bind the base framebuffer
		glctx.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{0})

		trans = mgl32.Ident4()
		draw(glctx, camera.ViewMatrix(), gallery[selection], trans, false, false)

		//trans = mgl32.Ident4()
		//trans = compose(mgl32.Scale3D(1.0,-1.0,1.0), trans)
		//draw(glctx, camera.ViewMatrix(), gallery[selection], trans, false, true)
	}
}

func paintIt(glctx gl.Context, camera mgl32.Mat4, trans mgl32.Mat4, name string, a attribs) {
	//log.Printf("Starting %v", name)
	var programUniforms uniforms
	if len(name) == 0 {
		//log.Printf("Empty name, refusing to draw")
		return
	}
	_, ok := vMeta[name]
	if !ok {
		//log.Printf("Object '%v' not found in object dictionary, not rendering", name)
		return
	}

	if a.useLighting {
		glctx.UseProgram(programLight)
		programUniforms = LightUniforms
	} else {
		glctx.UseProgram(program)
		programUniforms = BasicUniforms
	}

	if a.mirror {
		glctx.Uniform3fv(programUniforms.Mirror, []float32{1.0, -1.0, 1.0})
	} else {
		glctx.Uniform3fv(programUniforms.Mirror, []float32{1.0, 1.0, 1.0})
	}

	mvp := compose3(mgl32.Perspective(1.5707, float32(screenWidth)/float32(screenHeight), 0.001, 2048.0), camera, trans)
	mv := compose(camera, trans)
	glctx.UniformMatrix4fv(programUniforms.MVP, mvp[0:16])
	glctx.UniformMatrix4fv(programUniforms.ModelView, mv[0:16])
	glctx.UniformMatrix4fv(programUniforms.Model, trans[0:16])

	if name == "Quad" {
		//Add texture data
		glctx.BindBuffer(gl.ARRAY_BUFFER, quadTexAlignment)
		glctx.EnableVertexAttribArray(programUniforms.Texture)
		glctx.VertexAttribPointer(programUniforms.Texture, 2, gl.FLOAT, false, 0, 0)

		glctx.ActiveTexture(gl.TEXTURE0)
		// Bind the texture to this unit.
		glctx.BindTexture(gl.TEXTURE_2D, rtt_tex)
		//glctx.BindTexture(gl.TEXTURE_2D, gl.Texture{0})
		// Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
		glctx.Uniform1i(programUniforms.ActualTexture, 0)
		//Set x to 1.0 to use Textures in shader
		glctx.Uniform3fv(programUniforms.TextureOn, []float32{1.0, 0.0, 0.0})
	} else {
		glctx.Uniform3fv(programUniforms.TextureOn, []float32{0.0, 0.0, 0.0})
	}

	glctx.BindBuffer(gl.ARRAY_BUFFER, vBuffs[name])
	checkGlErr(glctx)
	glctx.EnableVertexAttribArray(programUniforms.Vertexes)
	checkGlErr(glctx)
	if ruleBookMeta()[name].backfaceCull {
		glctx.Enable(gl.CULL_FACE)
		if a.mirror {
			glctx.CullFace(gl.FRONT)
		} else {
			glctx.CullFace(gl.BACK)
		}
	} else {
		glctx.Disable(gl.CULL_FACE)
	}
	//fmt.Println(vMeta)
	//cpv := vMeta[name].coordsPerVertex
	//if cpv !=3 {
	//fmt.Printf("%v has invalid coordinates per vertex (%v)\n", name, cpv)
	//os.Exit(1)
	//}
	//if len(vTrisf[name]) != vMeta[name].vertexCount {
	//fmt.Printf("%v has invalid vertex count (%v) does not match triangle count %v\n", name, vMeta[name].vertexCount, len(vTrisf[name])/3)
	//    os.Exit(1)
	//}
	glctx.VertexAttribPointer(programUniforms.Vertexes, vMeta[name].coordsPerVertex, gl.FLOAT, false, 0, 0)
	checkGlErr(glctx)

	glctx.BindBuffer(gl.ARRAY_BUFFER, vColBuffs[name])
	checkGlErr(glctx)
	glctx.EnableVertexAttribArray(programUniforms.Colours)
	glctx.VertexAttribPointer(programUniforms.Colours, 4, gl.FLOAT, false, 0, 0)
	checkGlErr(glctx)
	glctx.BindBuffer(gl.ARRAY_BUFFER, vNormBuffs[name])
	checkGlErr(glctx)
	if a.useLighting {
		glctx.EnableVertexAttribArray(programUniforms.Normals)
		checkGlErr(glctx)
		glctx.VertexAttribPointer(programUniforms.Normals, 3, gl.FLOAT, false, 0, 0)
		checkGlErr(glctx)
	}
	//log.Printf("Drawing %v", name)
	glctx.DrawArrays(ruleBookMeta()[name].style, 0, vMeta[name].vertexCount)
	checkGlErr(glctx)
	glctx.DisableVertexAttribArray(programUniforms.Vertexes)
	checkGlErr(glctx)
	glctx.DisableVertexAttribArray(programUniforms.Colours)
	checkGlErr(glctx)
	if a.useLighting {
		glctx.DisableVertexAttribArray(programUniforms.Normals)
		checkGlErr(glctx)
	}

}

func draw(glctx gl.Context, camera mgl32.Mat4, aStr string, trans mgl32.Mat4, buildMode bool, mirrorOpt bool) ([]float32, []float32) {

	var setAngleRegex = regexp.MustCompile(`A([0-9.]+)`)
	var setHingeRegex = regexp.MustCompile(`Hinge\(([0-9]+)\)`)
	var setColourRegex = regexp.MustCompile(`Colour(\d\d?\d?),(\d\d?\d?),(\d\d?\d?)`)
	var setScaleRegex = regexp.MustCompile(`Scale\((-?[0-9.]+),(-?[0-9.]+),(-?[0-9.]+)\)`)

	start := s(aStr)
	//fmt.Printf("Start: %v\n", start)
	triBuf := []float32{}
	colBuf := []float32{}
	commands := start
	if buildMode {
		//fmt.Println(ruleBook())
		commands = runRuleset(start, ruleBook())
		trans = mgl32.Ident4()
	}
	//commands = runRules(commands, rules(), 2)

	forward := f32.Vec4{0, 1.0, 0, 0}
	//PI := float32(3.1415927)
	a := attribs{angle: float32(0.2), red: 1.0, blue: 1.0, green: 1.0, alpha: 1.0, mirror: mirrorOpt}
	stateStack := []mgl32.Mat4{}
	//fmt.Printf("Commands: %v\n", commands)

	for _, c := range commands {
		//fmt.Printf("Processing command: '%v'\n",c)
		//fmt.Println("Angle: ",angle)

		//fmt.Println(trans)
		switch {
		case c == "stem":
			trans = compose(trans, mgl32.Scale3D(1.0, 1.0, 1.0))
		case c == "F":
			trans = compose(trans, mgl32.Translate3D(forward[0], forward[1], forward[2]))
		case c == "f":
			trans = compose(trans, mgl32.Translate3D(-forward[0], -forward[1], -forward[2]))
		case c == "Y":
			trans = compose(trans, mgl32.HomogRotate3DZ(a.angle))
		case c == "R":
			trans = compose(trans, mgl32.HomogRotate3DY(a.angle))
		case c == "P":
			trans = compose(trans, mgl32.HomogRotate3DX(a.angle))
		case c == "y":
			trans = compose(trans, mgl32.HomogRotate3DZ(-a.angle))
		case c == "r":
			trans = compose(trans, mgl32.HomogRotate3DY(-a.angle))
		case c == "p":
			trans = compose(trans, mgl32.HomogRotate3DX(-a.angle))
		case c == "hs":
			trans = compose(trans, mgl32.Scale3D(0.5, 0.5, 0.5))
		case c == "hS":
			trans = compose(trans, mgl32.Scale3D(2.0, 2.0, 2.0))
		case c == "s":
			trans = compose(trans, mgl32.Scale3D(0.666, 0.666, 0.666))
		case c == "S":
			trans = compose(trans, mgl32.Scale3D(1.5, 1.5, 1.5))
		case c == "SY":
			trans = compose(trans, mgl32.Scale3D(1.0, 2.0, 1.0))
		case c == "[":
			stateStack = pushState(stateStack, trans)
			attribStack = pushAttribs(attribStack, a)
            //log.Printf("Push!\n")
		case c == "]":
			stateStack, trans = popState(stateStack, trans)
			attribStack, a = popAttribs(attribStack, a)
            //log.Printf("Pop!\n")
		case setHingeRegex.FindString(c) != "":
			var match = setHingeRegex.FindStringSubmatch(c)
			parsedNum, _ := strconv.ParseInt(match[1], 10, 32)
			num := int(parsedNum)
			trans = compose(trans, mgl32.HomogRotate3DY(hinges[num]))
		case c == "HR":
			trans = compose(trans, mgl32.HomogRotate3DY(clock*3.14159*2.0))
		case c == "rotateGreen":
			green += 0.05
			if green > 1 {
				green = 0.8
			}
		case setColourRegex.FindString(c) != "":
			////fmt.Println(c)
			var match = setColourRegex.FindStringSubmatch(c)
			parsedNum, _ := strconv.ParseFloat(match[1], 32)
			a.red = float32(parsedNum) / 255
			parsedNum, _ = strconv.ParseFloat(match[2], 32)
			a.green = float32(parsedNum / 255)
			parsedNum, _ = strconv.ParseFloat(match[3], 32)
			a.blue = float32(parsedNum / 255)
		case setAngleRegex.FindString(c) != "":
			var match = setAngleRegex.FindStringSubmatch(c)
			parsedNum, _ := strconv.ParseFloat(match[1], 32)
			a.angle = float32(parsedNum)
		case setScaleRegex.FindString(c) != "":
			var match = setScaleRegex.FindStringSubmatch(c)
			parsedNum, _ := strconv.ParseFloat(match[1], 32)
			x := float32(parsedNum)
			parsedNum, _ = strconv.ParseFloat(match[2], 32)
			y := float32(parsedNum)
			parsedNum, _ = strconv.ParseFloat(match[3], 32)
			z := float32(parsedNum)
			//fmt.Println("Scaling by ", x, y, z)
			trans = compose(trans, mgl32.Scale3D(x, y, z))
		case c == "reverseTriangle":
			reverse3(triBuf)
		case c == ".":
			//idVec := mgl32.Vec4{1,0,0,0}
			//newPoint := trans.Mul4x1(idVec)
			//triBuf = append(triBuf, newPoint[0], newPoint[1], newPoint[2])
			if buildMode {
				triBuf = append(triBuf, point(glctx, trans)...)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			}
		case c == "op":
			if buildMode {
				triBuf = append(triBuf, point(glctx, trans)...)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			}

		case c == "origin":
			if buildMode {
				idVec := mgl32.Vec4{0, 0, 0, 0}
				triBuf = append(triBuf, idVec[0], idVec[1], idVec[2])
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			}
		case c == "T":
			if buildMode {
				//glctx.Uniform4f(color, a.red, a.green, a.blue, 1)
				triBuf = append(triBuf, tr(glctx, trans)...)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			}
		case c == "Q":
			//if !buildMode {
			//glctx.Uniform4f(color, a.red, a.green, a.blue, 1)
			fmt.Printf("Pushing quad vertices\n")
			triBuf = append(triBuf, quad(glctx, trans)...)
			colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			//}
		case c == "TF":
			if buildMode {
				triBuf = append(triBuf, tr(glctx, trans)...)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
				colBuf = append(colBuf, a.red, a.green, a.blue, a.alpha)
			}
			trans = compose(trans, mgl32.Translate3D(forward[0], forward[1], forward[2]))
		case c == "LightsOn":
			a.useLighting = true
		case c == "LightsOff":
			a.useLighting = false
		case c == "mirrorOn":
			a.mirror = true
			//camera = compose(mgl32.Scale3D(1.0,-1.0,1.0), camera)
		case c == "mirrorOff":
			a.mirror = false
			//camera = compose(mgl32.Scale3D(1.0,-1.0,1.0), camera)
		default:
			if !buildMode {
				_, ok := modelReady.Lookup(c)
				if ok == nil {
					paintIt(glctx, camera, trans, c, a)
				}
			}
		}
	}
	return triBuf, colBuf
}

func checkGlErr(glctx gl.Context) {
	err := glctx.GetError()
	if err > 0 {
		fmt.Printf("GLerror: %v\n", err)
		//panic("GLERROR")
	}
}

//func test () {
//vec := mgl32.Vec4{, float32(triangleData[1]), float32(triangleData[2]), 1}
//p1 := trans.Mul4x1(vec)
//}

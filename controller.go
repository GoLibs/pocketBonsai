// +build !VR

package main

import (
	"log"

	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/exp/sensor"
)

func handleSensor(e sensor.Event) {
	log.Println(e)
}

func handleEvent(a app.App, i interface{}) {
	//log.Println(i)
	switch e := a.Filter(i).(type) {
	case key.Event:
		switch e.Code {
		case key.CodeS:
			camera.Translate(0.0, 0.0, -0.1)
		case key.CodeW:
			camera.Translate(0.0, 0.0, 0.1)
		case key.CodeA:
			camera.Translate(0.1, 0.0, 0.0)
		case key.CodeD:
			camera.Translate(-0.1, 0.0, 0.0)
		case key.CodeQ:
			camera.RotateY(-0.1)
		case key.CodeE:
			camera.RotateY(0.1)
		}
	}
}

//2016/09/24 12:59:48 key.Event{'s' (CodeS), key.Modifiers(), Release}

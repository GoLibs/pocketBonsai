// shaders
package main

import "golang.org/x/mobile/gl"

func getUniforms(glctx gl.Context, program gl.Program) uniforms {
	var u uniforms
	u.Vertexes = glctx.GetAttribLocation(program, "position")
	u.Colours = glctx.GetAttribLocation(program, "colour")
	u.Normals = glctx.GetAttribLocation(program, "normal")
	u.Texture = glctx.GetAttribLocation(program, "a_TexCoordinate")
	u.MVP = glctx.GetUniformLocation(program, "transform")
	u.ModelView = glctx.GetUniformLocation(program, "modelView")
	u.Model = glctx.GetUniformLocation(program, "model")
	u.Mirror = glctx.GetUniformLocation(program, "mirror")
	u.TextureOn = glctx.GetUniformLocation(program, "textureOn")
	u.Ambient = glctx.GetUniformLocation(program, "ambient")
	u.ActualTexture = glctx.GetUniformLocation(program, "u_Texture")
	return u
}

const basicVertexShader = `#version 100 
precision lowp float;
    uniform vec2 offset;
    uniform mat4 transform;
    uniform mat4 modelView;
    uniform mat4 model;
    uniform vec3 mirror;
    uniform vec3 textureOn;
    varying vec3 mirrorS;


    attribute vec4 position;
    attribute vec4 colour;
    attribute vec3 normal;
    attribute vec2 a_TexCoordinate;
    varying vec2 v_TexCoordinate;

    varying vec4 color;
    varying vec3 N;
    varying vec3 v;
    varying float texOn;
    varying vec3 modelPos;
    void main() {
        modelPos = vec3(model * position);
        mirrorS = mirror;
        vec4 pos = transform * position;
        color = colour;
        gl_Position = pos;
        v_TexCoordinate = a_TexCoordinate;
        if (textureOn.x>0.5) {
            texOn = 1.0;
        } else {
            texOn = -1.0;
        }
    }
`

const vertexShader = `#version 100 
precision highp float;
    uniform vec2 offset;
    uniform mat4 transform;
    uniform mat4 modelView;
    uniform mat4 model;
    uniform vec3 mirror;
    //uniform vec3 textureOn;
    vec3 textureOn = vec3(1,1,1);
    varying vec3 mirrorS;


    attribute vec4 position;
    attribute vec4 colour;
    attribute vec3 normal;
    attribute vec2 a_TexCoordinate;
    varying vec2 v_TexCoordinate;

    varying vec4 color;
    varying vec3 N;
    varying vec3 v;
    varying vec3 modelPos;
    void main() {
        modelPos = vec3(model * position);
        mirrorS = mirror;
        vec4 pos = transform*position;
        vec3 norm = normal;
        //vec4 col = vec4(norm.x, norm.y, norm.z, 1.0);
        color = colour;

        v = vec3(modelView * position);
        vec4 N4 = normalize(modelView * vec4(norm.x, norm.y, norm.z,0.0));
        N = N4.xyz;

        gl_Position = pos;
        if (textureOn.x>0.5) {
            v_TexCoordinate = a_TexCoordinate;
        } else {
            v_TexCoordinate = vec2(-1.0,-1.0);
        }
    }
`

const basicFragmentShader = `#version 100
precision lowp float;
varying vec3 N;
varying vec3 v;
varying vec4 color;
varying vec3 mirrorS;
uniform sampler2D u_Texture;    // The input texture.
uniform vec4 ambient;
varying vec2 v_TexCoordinate; // Interpolated texture coordinate per fragment.
vec3 LightPosition = vec3(0,0,1);
varying vec3 modelPos;
varying float texOn;
void main() {
   vec3 m = modelPos * mirrorS;
   float filter = 1.0;
    if (texOn<0.0) {
            gl_FragColor = filter*color;
    } else {
        //gl_FragColor = filter*texture2D(u_Texture, v_TexCoordinate);
        if (v_TexCoordinate.x<0.1) {
            gl_FragColor = vec4(1.0,0.0,0.0,1.0);
        } else {
            gl_FragColor = texture2D(u_Texture, v_TexCoordinate);
        }
    }
}`

const fragmentShader = `#version 100
precision highp float;
varying vec3 N;
varying vec3 v;
varying vec4 color;
varying vec3 mirrorS;
uniform sampler2D u_Texture;    // The input texture.
uniform vec4 ambient;
varying vec2 v_TexCoordinate; // Interpolated texture coordinate per fragment.
vec3 LightPosition = vec3(0.3,0.3,0);
varying vec3 modelPos;
void main() {
   vec3 m = modelPos * mirrorS;
   float filter = 0.5;
   vec3 L = normalize(LightPosition.xyz - v);
   float Idiff = max(abs(dot(N,L)), 0.1);
   Idiff = clamp(Idiff, 0.0, 1.0); 
   if (v_TexCoordinate.x<0.0) {
       gl_FragColor = vec4(0.01)*color + filter*color*vec4(Idiff, Idiff, Idiff, 1.0);
   } else {
       //gl_FragColor = vec4(v_TexCoordinate.x, v_TexCoordinate.y, 0.0, 1.0) + filter*texture2D(u_Texture, v_TexCoordinate)*;
    gl_FragColor = color*vec4(Idiff, Idiff, Idiff, 1.0);
        //gl_FragColor = vec4(1.0,0.0,0.0,1.0);
   }
}`

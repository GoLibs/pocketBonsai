// models
package main

import "golang.org/x/mobile/exp/f32"
import "encoding/binary"
import "golang.org/x/mobile/gl"

var vMeta map[string]vertexMeta
var vTris map[string][]byte
var vTrisf map[string][]float32
var vBuffs map[string]gl.Buffer

var vCols map[string][]byte
var vNorms map[string][]byte
var vColsf map[string][]float32
var vNormsf map[string][]float32
var vColBuffs map[string]gl.Buffer
var vNormBuffs map[string]gl.Buffer

var quadTexAlignment gl.Buffer

var triangleData = f32.Bytes(binary.LittleEndian,
	0.0, 1.0, 0.0, // top left
	-0.1, 0.0, 0.0, // bottom left
	0.1, 0.0, 0.0, // bottom right
	//0.2, 1.0, 0.0, // top right
	//0.0, 1.0, 0.0, // top left
	//0.2, 0.0, 0.0, // bottom right
//
//0.0, 1.0, 0.0, // top left
//0.0, 0.0, 0.0, // bottom left
//0.0, 0.0, 0.2, // bottom right
//0.0, 1.0, 0.2, // top right
//0.0, 1.0, 0.0, // top left
//0.0, 0.0, 0.2, // bottom right
)

var texAlignData = f32.Bytes(binary.LittleEndian,
	0.0, 0.0, // top left
	0.0, 1.0, // top left
	1.0, 0.0, // top left
	0.0, 1.0, // top left
	1.0, 1.0, // top left
	1.0, 0.0, // top left
)

type vertexMeta struct {
	coordsPerVertex int
	vertexCount     int
}

const (
	coordsPerVertex = 3
	vertexCount     = 3
)

var texData = f32.Bytes(binary.LittleEndian,
	0.0, 1.0, 1.0, // top left
	1.0, 0.0, 0.0, // bottom left
	1.0, 0.0, 0.0, // bottom right
	0.0, 0.0, 1.0, // bottom right
	1.0, 0.0, 0.0, // top left
	0.0, 0.0, 0.0, // bottom right
//
/*0.0, 1.0, 0.0, // top left
  0.0, 0.0, 0.0, // bottom left
  0.0, 0.0, 1.0, // bottom right
  0.0, 1.0, 1.0, // top right
  0.0, 1.0, 0.0, // top left
  0.0, 0.0, 0.9, // bottom right
  0.0, 0.0, 0.9, // bottom right
  0.0, 1.0, 0.9, // top right
  0.0, 1.0, 0.0, // top left
  0.0, 0.0, 0.9, // bottom right
*/
)

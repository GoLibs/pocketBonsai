// Copyright 2014 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build darwin linux windows

// An app that draws a green triangle on a red background.
//
// Note: This demo is an early preview of Go 1.5. In order to build this
// program as an Android APK using the gomobile tool.
//
// See http://godoc.org/golang.org/x/mobile/cmd/gomobile to install gomobile.
//
// Get the basic example and use gomobile to build or install it on your device.
//
//   $ go get -d golang.org/x/mobile/example/basic
//   $ gomobile build golang.org/x/mobile/example/basic # will build an APK
//
//   # plug your Android device to your computer or start an Android emulator.
//   # if you have adb installed on your machine, use gomobile install to
//   # build and deploy the APK to an Android target.
//   $ gomobile install golang.org/x/mobile/example/basic
//
// Switch to your device or emulator to start the Basic application from
// the launcher.
// You can also run the application on your desktop by running the command
// below. (Note: It currently doesn't work on Windows.)
//   $ go install golang.org/x/mobile/example/basic && basic
package main

import (
	"log"

	"os"
	"time"

	"github.com/donomii/sceneCamera"
	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/app/debug"

	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/gl"
    "fmt"
)
import "github.com/go-gl/mathgl/mgl32"
import "golang.org/x/mobile/exp/sensor"

var (
	images       *glutil.Images
	fps          *debug.FPS
	program      gl.Program
	programLight gl.Program
	buf          gl.Buffer

	screenWidth  int
	screenHeight int

	texWidth  int
	texHeight int

	green        float32
	red          float32
	blue         float32
	touchX       float32
	touchY       float32
	selection    int
	gallery      []string
	reCalcNeeded bool
	prevTime     int64
	camera       *sceneCamera.SceneCamera
    hinges       []float32
    frog       []float32
    tapering   []float32
    snowflake   []float32
    terrier    []float32
    rooster    []float32
    goldfish   []float32
    snek       []float32
    ball       []float32
    bow        []float32
)

//The simple shaders just need Vertexes and MVP, while the more complicated shaders need all the separate pieces to do lighting calculations.  Luckily GL optimises out unused uniforms, so we just declare everything at the top for every shader.

type uniforms struct {
	MVP           gl.Uniform
	Model         gl.Uniform
	View          gl.Uniform
	ModelView     gl.Uniform
	Mirror        gl.Uniform
	TextureOn     gl.Uniform
	Ambient       gl.Uniform
	ActualTexture gl.Uniform
	Texture       gl.Attrib
	Colours       gl.Attrib
	Normals       gl.Attrib
	Vertexes      gl.Attrib
}

var BasicUniforms uniforms
var LightUniforms uniforms

func resetCam() {
	camera.Reset()
	camera.Translate(0.0, 0.2, 0.0)
	camera.LookAt(0.0, 0.0, 0.0)
}

func transform() {
        rubiks := [][]float32{tapering, snowflake, rooster, terrier,frog, bow, goldfish, ball}
        for _,critter := range rubiks {
            for i,v := range critter {
                if hinges[i] != (v+2)*3.141527/2.0 {
                time.Sleep(2*time.Second)
                start_linear_animation(&hinges[i], hinges[i], (v+2)*3.141527/2.0, 1.0)
                }
                //hinges[i] = (v+2)*3.141527/2.0  //+2 because the prism primitive leaves the rotation "upside down"
            }
            start_linear_animation(&clock, 0.0, 1.0, 10.0)
            time.Sleep(10.0*time.Second)
        }

        transform()
}

func main() {
	gallery = []string{
		`[ mirrorOff noRtt LightsOn Colour255,0,0 f A0.35 R HR [ s s
            Arrow F Arrow Prism [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
            A1.5707
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(0) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(1) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(2) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(3) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(4) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(5) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(6) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(7) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(8) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(9) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(10) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(11) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(12) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(13) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(14) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(15) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(16) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(17) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(18) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(19) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(20) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(21) Prism  [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
                hs F Scale(1.2,1.2,1.2) p F Scale(0.8333,0.8333,0.8333) hS Hinge(22) Prism1 [ hs F  A2.356 P  Scale(1.0,1.2,1.0) Qua ]
    ]
]

`,
		`[ s s s s s s A1.5707 R R Y F y f f s s s HR HR [ [ P Circle ] Plant ] ]`,
		//[ mirrorOn Scale(-1.0,1.0,1.0) s s s s s A1.5707 R R Y F y f f s s s HR HR [ [ P Circle ] Plant ] mirrorOff ]`,

		`s s s s [ [                 [ A1.5707 Colour255,0,0 [ Tetrahedron ] P [ P P HR Circle ] [ P HR Circle ] HR Tetrahedron ] ] ]`,
		//[ mirrorOn Scale(1.0,-1.0,1.0) A1.5707 Colour255,0,0 LightsOn [ Tetrahedron ] P [ P P Circle ] [ P Circle ] Tetrahedron mirrorOff ] ] ]`,

		`s s s s HR HR HR HR [ s F F                            A1.5707 Colour255,0,0 LightsOn Icosahedron LightsOff ]`,

		"A1.5707 HR S [ Arrow Y P ] Y Y Arrow",
		"A1.5707 Colour255,0,0 s s s s s s HR HR HR ilake",
		"s HR s s s F FlowerField",
		"s s s s s s s starburst",
		"A1.5707 Colour255,0,0 s s s s s s HR orient",
		`[ mirrorOff noRtt Colour255,0,0 HR HR HR HR s s s A0.7 [ P F F Quad ] ]`,
		"A1.5707 Colour255,0,0 P HR P HR Square1",
		"A1.5707 Colour255,0,0 HR P HR Face",
		"A1.5707 Colour255,0,0 HR P HR Arrow",
		"A1.5707 s s R R P A0.7 P Arrow",
		"A1.5707 s s s s R R A0.785 P Flower",
		"A1.5707 s s s s s [ R R s Flower11 ] [ Y F F y R R s Flower11 ] [ y F F Y R R s Flower10 ]",
		"A1.57 S HR R s s s s s s [ Colour0,255,0 3DTree3 ] A1.57 [ Y F F F F F F F F F F y s r Colour255,0,0  3DTreeLeafy ] A1.57 [ y F F F F F F F F F F Y s r r Colour0,0,255 3DTreeLeafy ] Colour0,255,255 A1.57  [ S P [ leaf2 ] A1.57 Y Y [ leaf2 ] ] ",
		"A1.57 R [ s s F F F F F F F F P s s Colour200,200,200 ] H R [ s Colour0,0,200 [ S S S square starburst ] ] s s s s s [ Colour0,255,0 ] A1.57 [ Y F F F F F F F F F F y s r Colour255,0,0  ] A1.57 [ y F F F F F F F F F F Y s r r Colour0,0,255 ] Colour0,255,255 A1.57  [ S P [ leaf2 ] A1.57 Y Y [ leaf2 ] ] ",
		"A1.57 R R HR R s [ s Colour0,0,50 starburst ] s s s s s  A1.57  A1.57  Colour0,255,255 A1.57  [ S P  A1.57 Y Y [ leaf2 ] ] ",
		"s s s A1.50 Colour255,0,0 starburst Y Y starburst P F F F Colour0,255,0 starburst Y Y starburst",
		"HR s s s s s s 3DTreeLeafy",
		"s s s s s s s Tree3",
		"A1.57 s s s s lineStar",
		"A1.57 s s s s leaf",
		"A1.57 s s s s leaf2",
		"A0.7 R Y Y s s s s s s s s s s Koch3",
		"A0.7 R P Y s s s s s s s Gosper",
		"A0.7 R P s s s s s s s s Sierpinksi",
		"s s s s s s s s Koch2",
		"s s s KIomega",
	}
	app.Main(func(a app.App) {
		reCalcNeeded = true
		var glctx gl.Context
		var sz size.Event
		sensor.Notify(a)
		vTris = map[string][]byte{}
		vCols = map[string][]byte{}
		vNorms = map[string][]byte{}
		vColsf = map[string][]float32{}
		vNormsf = map[string][]float32{}
		vTrisf = map[string][]float32{}
		vBuffs = map[string]gl.Buffer{}
		vColBuffs = map[string]gl.Buffer{}
		vNormBuffs = map[string]gl.Buffer{}
		vMeta = map[string]vertexMeta{}
		vTris["fakeCylinder"] = triangleData
		vMeta["fakeCylinder"] = vertexMeta{coordsPerVertex: coordsPerVertex, vertexCount: vertexCount}
		camera = sceneCamera.New()
        hinges = []float32{2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5,2*3.1415927*0.5}
        //start_clock(&clock, 0.0, 1.0, 10.0)
        //start_linear_animation(&hinges[1], 0.0, 10.0, 100.0)
        frog = []float32{-1,0,-1,1,1,2,-1,1,0,0,1,2,-1,0,0,-1,1,2,-1,-1,1,0,1}
        tapering = []float32{0,1,0,2,2,0,-1,2,-1,-1,0,0,0,1,-1,2,-1,0,2,2,0,-1,0}
        snowflake = []float32{-1,-1,-1,-1,1,1,1,1,-1,-1,-1,-1, 1,1,1,1,-1,-1,-1,-1,1,1,1,1}
        rooster = []float32{2,0,-1,0,-1,1,2,1,0,2,2,0,1,2,1,-1,0,-1,0,2,2,0,0}
        terrier = []float32{2,0,2,2,0,2,0,0,0,2,2,0,2,0,0,2,0,2,2,0,0,0,0}
        ball = []float32{-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1}
        snek = []float32{0,1,0,2,2,0,2,2,0,1,0,0,0,2,-1,-1,2,-1,0,0,2,1,-1}
        goldfish = []float32{0,0,2,0,2,0,0,2,-1,1,1,2,-1,1,0,1,-1,2,1,1,-1,0,2}
        bow =  []float32{-1,-1,-1,1,-1,1,1,1,-1,-1,-1,1,-1,1,1,1,-1,-1,-1,1,-1,1}
        for i,v := range snek {
            hinges[i] = (v+2)*3.141527/2.0  //+2 because the prism primitive leaves the rotation "upside down"
        }
        go transform()
		for e := range a.Events() {
			handleEvent(a, e)
			switch e := a.Filter(e).(type) {
			case sensor.Event:
				handleSensor(e)
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					glctx, _ = e.DrawContext.(gl.Context)
					onStart(glctx)
					sensor.Enable(sensor.Gyroscope, 10*time.Millisecond)
					a.Send(paint.Event{})
				case lifecycle.CrossOff:
					sensor.Disable(sensor.Gyroscope)
					onStop(glctx)
					glctx = nil
				}
			case size.Event:
				sz = e
				reCalcNeeded = true
				screenWidth = sz.WidthPx
				screenHeight = sz.HeightPx
				touchX = float32(sz.WidthPx / 2)
				touchY = float32(sz.HeightPx * 9 / 10)
				if sz.Orientation == size.OrientationLandscape {
					//threeD = true
				} else {
					threeD = false
				}
			case paint.Event:
				if glctx == nil || e.External {
					// As we are actively painting as fast as
					// we can (usually 60 FPS), skip any paint
					// events sent by the system.
					continue
				}

				onPaint(glctx, sz)
				a.Publish()
				// Drive the animation by preparing to paint the next frame
				// after this one is shown.
				//time.Sleep(1 * time.Millisecond)
				a.Send(paint.Event{})
			case touch.Event:
				//log.Println(e)
				if e.Type == touch.TypeBegin {
					resetCam()
					reCalcNeeded = true
					selection++
					if selection+1 > len(gallery) {
						selection = 0
					}
				}
			}
		}
	})
}

func onStart(glctx gl.Context) {
	var err error
	texWidth = 64
	texHeight = 64
	program, err = glutil.CreateProgram(glctx, basicVertexShader, basicFragmentShader)
	if err != nil {
		log.Printf("error creating GL program: %v", err)
		os.Exit(1)
		return
	}

	BasicUniforms = getUniforms(glctx, program)

	programLight, err = glutil.CreateProgram(glctx, vertexShader, fragmentShader)
	if err != nil {
		log.Printf("error creating GL program: %v", err)
		os.Exit(1)
		return
	}

	LightUniforms = getUniforms(glctx, programLight)

	images = glutil.NewImages(glctx)
	fps = debug.NewFPS(images)

	L_init()

	quadTexAlignment = buildTexBuff(glctx)
	init_textures(glctx)
	//Eventually we will be able to run buildAll in its own background thread
	//But now it causes crashes while updating hashmaps
	//All the hashmaps have to go (to be replaced with symbols)
	buildAll(glctx)
	Rtt(glctx)
    //string2Tex(glctx, "Hello", gl.Texture{1})
    string2Tex(glctx, "Hello", rtt_tex)
}

func onStop(glctx gl.Context) {
	glctx.DeleteProgram(program)
	glctx.DeleteBuffer(buf)
	fps.Release()
	images.Release()
}

func transpose(m mgl32.Mat4) mgl32.Mat4 {
	var r mgl32.Mat4
	for i, v := range []int{0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15} {
		r[i] = m[v]
	}
	//fmt.Println(r)
	return r
}

var version = 1
func onPaint(glctx gl.Context, sz size.Event) {
	glctx.ClearColor(1, 1, 0, 1)
	glctx.Clear(gl.COLOR_BUFFER_BIT)
	red = 0.0
	green = 1.0
	blue = 1.0

	glctx.UseProgram(program)

	lsysPaint(glctx, sz)
    //screenShot(glctx, fmt.Sprintf("screenshot%v.png", version))
    version = version +1
	fps.Draw(sz)
}
